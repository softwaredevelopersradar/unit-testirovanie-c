﻿

using NUnit.Framework;

namespace ОбзорВозможностей.Tests
{
    [TestFixture]
    public class HumanTest
    {
        [Test]
        public void CheckNegativeCoordInMoveToMethod()
        {
            Coord coord = new Coord();
            coord.lat = -1;
            coord.lon = -1;
            coord.alt = -1;
            Human human = new Human();
            
            Assert.IsFalse(human.MoveTo(coord));
        }

        [Test]
        public void CheckCorrectCoordInMoveToMethod()
        {
            Coord coord = new Coord();
            coord.lat = 1;
            coord.lon = 1;
            coord.alt = 1;
            Human human = new Human();

            Assert.IsTrue(human.MoveTo(coord));
        }

        [TestCase(120,100,220)]
        [TestCase(-100, 0, -100)]
        [TestCase(0,0,0)]
        public void CheckSumMethod(int number1, int number2, int expectedSum)
        {
            Human human = new Human();

            int sum = human.Sum(number1, number2);
            Assert.AreEqual(expectedSum, sum);
        }
    }
}
